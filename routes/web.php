<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/self-isolation', function () {
    return view('self-isolation');
});

Route::get('/self-assessment', function () {
    return view('self-assessment');
});



Route::get('/form', function () {
    return view('form');
});

Route::get('/statistics', 'StatsController@getKenyaCases');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');


Route::get('hospital/home', 'HomeController@hospitalHome')->name('hospital.home')->middleware('is_hospital');

Route::get('charity/home', 'HomeController@charityHome')->name('charity.home')->middleware('is_charity');