<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Resources;
use Illuminate\Http\Resources\Json\Resource;

class StatsController extends Controller
{
    public function getKenyaCases()
    {
        $response = Http::get("https://corona.lmao.ninja/v2/countries/kenya");
         $cases= $response->json();
         
         return View('statistics', ["cases"=>$cases]);
    
}
}
