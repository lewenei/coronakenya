<!DOCTYPE html>
<html lang="en">

@include('head')

<body>

  <!-- ======= Header ======= -->
@include('header')
  <!-- End Header -->
    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center">

        <div class="container">
          <div class="row">
            <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1" data-aos="fade-up">
              <div>
                <h1>These are the Current Statistics of Cases in Kenya</h1>
               
              </div>
            </div>
            <div class="col-lg-6 d-lg-flex flex-lg-column align-items-stretch order-1 order-lg-2 hero-img" data-aos="fade-up">
              <img src="img/hero-img.png" class="img-fluid" alt="">
            </div>
          </div>
        </div>
    
      </section><!-- End Hero -->


     
  <!-- ======= Contact Section ======= -->
  <section id="contact" class="contact">
    <div class="container">

     

      <div class="row">

        <div class="col-lg-10">
          <div class="row">
            <div class="col-lg-6 info" data-aos="fade-up">
              <i class="bx bx-trending-up"></i>
              <h4>Total Cases</h4>
            <p>{{ $cases["cases"] }}</p>
            </div>
            <div class="col-lg-6 info" data-aos="fade-up" data-aos-delay="100">
              <i class="bx 
              bxs-ambulance"></i>
              <h4>Cases Today</h4>
              <p>{{ $cases["todayCases"] }}</p>
            </div>
            <div class="col-lg-6 info" data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-trending-down"></i>
              <h4>Recovered Cases</h4>
              <p>{{ $cases["recovered"] }}</p>
            </div>
            <div class="col-lg-6 info" data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-plus-medical"></i>
              <h4>Death</h4>
              <p>{{ $cases["deaths"] }}</p>
            </div>
          </div>
        </div>



      </div>

    </div>
  </section><!-- End Contact Section -->

 <!-- ======= Footer ======= -->
@include('footer')
 <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a><!-- Vendor JS Files -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/php-email-form/validate.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/venobox/venobox.min.js"></script>
<script src="vendor/aos/aos.js"></script>

<!-- Template Main JS File -->
<script src="js/main.js"></script>


<!--Json Data-->
<script>
  $.getJSON('https://https://corona.lmao.ninja/v2/countries/kenya', function(data) {
      var text = ``
      $(".mypanel").html(text);
  });
</script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

</body>

</html>