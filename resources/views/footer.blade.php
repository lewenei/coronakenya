<footer id="footer">



    <div class="container py-4">
      <div class="copyright">
        &copy; Copyright <strong><span>CoronaKenya</span></strong>
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-bootstrap-app-landing-page-template/ -->
        Designed by <a href="https://aitihub.tech/">aITiHub Solutions Kenya</a>
      </div>
    </div>
  </footer>