<!DOCTYPE html>
<html lang="en">

@include('head')

<body>

  <!-- ======= Header ======= -->
@include('header')
    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center">

        <div class="container">
          <div class="row">
            <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1" data-aos="fade-up">
              <div>
                <h1>COVID-19 Self-assessment and Resource App</h1>
                <h2>This online tool helps determine whether you need to be tested for COVID-19. 
                    You can complete this assessment for yourself or on behalf of someone else, if they are not able.</h2>
                    <h2>Hospitals can also use this tool to request for materials including facemasks, ventilators, test kits and even employees.</h2>
                    <h2>Charities helping people cope with COVID-19 can use this tool to request for financial help.</h2>
                <a href="{{ url ('/hospital/home')}}" class="download-btn"><i class="bx bxs-ambulance"></i> Hospital</a>
                <a href="{{ url ('/charity/home')}}" class="download-btn"><i class="bx bxs-heart"></i> Charity</a>
              </div>
            </div>
            <div class="col-lg-6 d-lg-flex flex-lg-column align-items-stretch order-1 order-lg-2 hero-img" data-aos="fade-up">
              <img src="img/hero-img.png" class="img-fluid" alt="">
            </div>
          </div>
        </div>

        
    
      </section><!-- End Hero -->
   

    <!-- ======= Footer ======= -->
   @include('footer')
    <!-- End Footer -->


      <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

      <!-- Vendor JS Files -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
      <script src="vendor/php-email-form/validate.js"></script>
      <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
      <script src="vendor/venobox/venobox.min.js"></script>
      <script src="vendor/aos/aos.js"></script>
    
      <!-- Template Main JS File -->
      <script src="js/main.js"></script>
    
    </body>
    
    </html>