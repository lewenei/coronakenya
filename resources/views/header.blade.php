<header id="header" class="fixed-top">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href="{{ url ('/')}}">Corona Kenya</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="{{ url ('/')}}">Home</a></li>
          <li><a href="{{ url ('/statistics')}}">Statistics</a></li>
          
        
          <li><a href="{{ url ('/self-isolation')}}">Self-Isolation</a></li>

        

          <li class="get-started"><a href="{{ url ('/self-assessment')}}">Self Assessment</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->